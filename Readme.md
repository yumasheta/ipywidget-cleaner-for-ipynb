```
Note:  
This project is incomplete and development has stalled.
```


# `ipywidget` output cleaner pre-commit hook

This repo is meant to be used with the [pre-commit](https://pre-commit.com) tool.
The hook runs for Jupyter notebook files that are about to be commited and replaces any output capturing ipywidgets by direct output.
The idea is, that this way the notebook's output can be viewed without needing to reconstruct the widgets.

The code in this repo was written by [ChatGPT Dec 15 Version](https://chat.openai.com) and improved by the contributors.
The logs of all chat sessions with ChatGPT are found in the `./chatlogs/` folder.


## Installation

To use this hook with pre-commit, add the following to your ".pre-commit-config.yaml" file:

```
repos:
-   repo: https://gitlab.com/yumasheta/ipywidget-cleaner-for-ipynb
    rev: master
    hooks:
    -   id: ipywidget-cleaner
```

Install pre-commit:
```bash
pip install pre-commit
```

Then, run the following command to install the hooks:

```bash
pre-commit install
```

From now on, pre-commit will run the "ipywidget-cleaner" hook on your code whenever you commit changes.

## Usage

To use the "ipywidget-cleaner" hook, simply commit your changes as you normally would. The hook will automatically run on your code and clean any ipywidgets it finds.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for more details.
