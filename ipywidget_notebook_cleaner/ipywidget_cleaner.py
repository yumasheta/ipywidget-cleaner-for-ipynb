#!/usr/bin/env python3

"""Cleans up notebooks with ipywidgets.

Removes ipywidgtes from notebooks.

Written by OpenAI ChatGPT Dec. 15 Version.
Modified by Kayran Schmidt.
"""

# TODO: accept argument (using argparse), so it works as a pre-commit script
#	is argument passed when using as hook?
# TODO: ipywidgets are commented out, but not replaced. the notebook will not execute

import nbformat
import os.path
from nbconvert.preprocessors import ExecutePreprocessor

def run():
    # Set the path to the original notebook
    original_notebook = 'example.ipynb'

    # Initialize an empty set to store the imported modules
    imported_modules = set()

    # Load the notebook from a file
    with open(original_notebook, 'r') as f:
        nb = nbformat.reads(f.read(), as_version=4)

    # Check if the notebook was executed before
    if 'execution_count' in nb['metadata']:
        # Iterate over the cells in the notebook
        for cell in nb['cells']:
            # Check if the cell is a code cell
            if cell['cell_type'] == 'code':
                # Split the cell's source code into lines
                lines = cell['source'].split('\n')
                # Iterate over the lines of code
                for i, line in enumerate(lines):
                    # Strip leading whitespace from the line
                    line = line.lstrip()

                    # Check if the line starts with 'from ipywidgets import' or 'import ipywidgets'
                    if line.startswith('from ipywidgets import') or line.startswith('import ipywidgets'):
                        # Extract the imported module(s) from the line
                        module = line.split('import')[1].strip()
                        # Add the module(s) to the set
                        imported_modules.update(module.split(','))
                        # comment out the line
                        lines[i] = "#" + lines[i]
                # Rejoin the modified lines of code into a single cell
                cell['source'] = '\n'.join(lines)

    # Get the kernelspec from the metadata of the original notebook
    kernelspec = nb['metadata'].get('kernelspec', {})

    # Create an ExecutePreprocessor instance
    ep = ExecutePreprocessor(timeout=240, kernel_name=kernelspec.get('name'), kernel_argv=kernelspec.get('argv'))

    # Get the absolute path of the original notebook
    path = os.path.abspath(original_notebook)

    # Set the path in the metadata to the absolute path of the original notebook
    metadata = {'metadata': {'path': path}}

    # Execute the modified notebook
    ep.preprocess(nb, metadata)

    # Overwrite the original notebook file with the modified and executed notebook
    with open(original_notebook, 'w') as f:
        f.write(nbformat.writes(nb))

if __name__ == "__main__":
    run()
